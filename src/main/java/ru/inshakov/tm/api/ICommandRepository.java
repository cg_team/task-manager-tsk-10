package ru.inshakov.tm.api;

import ru.inshakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
